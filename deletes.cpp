#include "deletes.h"

void destroy_graph(Grafo* grafo){
    int i, size = grafo->getVertices().size();
    for(i = 0; i < size; i++){
        delete grafo->getVertices().at(i);
    }
    delete grafo;
}