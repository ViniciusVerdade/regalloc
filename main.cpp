#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "args_handler.h"
#include "deletes.h"
#include "coloring.h"
#include "Grafo.h"
#include "Vertice.h"

using namespace std;

int main(int argc, const char *argv[])
{
	string arg_input;
    vector<string> file_lines;
    //int i = 0;

    //PEGANDO TODAS AS LINHAS DO ARQUIVO DE ENTRADA
	while (getline(cin, arg_input)){
		file_lines.push_back(arg_input);
    }

    //Grafo n:
    string nome_grafo = file_lines.at(0);
    vector<string> name_separator = splitComDelimitador(nome_grafo," ");
    nome_grafo = "Graph " + name_separator.at(1);
    nome_grafo.pop_back();
    // K = algum número
    int cores_disponiveis = numeroDeCores(file_lines.at(1));
    int cores_enviado = cores_disponiveis;

    vector<bool> is_success;
    vector<bool>::iterator beg_success;

    Grafo *grafo;

    //    https://www.youtube.com/watch?v=CCuxv2O1IIA&list=RDQ_NB5luxtic&index=9
    //tira as linhas da nomeação do grafo e da definição de k
    file_lines.erase(file_lines.begin(),file_lines.begin()+2);
    cout << nome_grafo <<" -> Physical Registers: "<< cores_disponiveis <<endl;
    cout << "----------------------------------------"<<endl;
    //LOOP COMEÇA AQUI
    while(cores_disponiveis>=2){
        //todo FAZER O LOOP DE EXECUÇÕES ATÉ K=2
        cout << "----------------------------------------"<<endl;
        beg_success = is_success.begin();

        //*etapa de build
        grafo = montandoGrafoEntrada(file_lines,nome_grafo,cores_disponiveis,cores_enviado);

        //*etapa de simplify
        vector<Vertice *> pilha_de_remocao = simplify(grafo,cores_disponiveis,cores_enviado);
        if(cores_disponiveis == 5){
            //grafo->printElementosGrafo();
        }
        //*etapa de select and assign
        bool success = select_assign(grafo, pilha_de_remocao, cores_disponiveis,cores_enviado);
        is_success.insert(beg_success,success) ;

        destroy_graph(grafo);

        cores_disponiveis--;
    }

    cout << "----------------------------------------"<<endl;
    cout << "----------------------------------------"<<endl;
    int i,size = is_success.size();
    for(i = size-1; i >= 0; i--){
        if(is_success.at(i)){
            if(cores_enviado < 10){
                cout<< nome_grafo <<" -> K =  " << cores_enviado<<": Successful Allocation";
            }else{
                cout<< nome_grafo <<" -> K = " << cores_enviado<<": Successful Allocation";
            }
        }else{
            if(cores_enviado < 10){
                cout<< nome_grafo <<" -> K =  " << cores_enviado<<": SPILL";
            }else{
                cout<< nome_grafo <<" -> K = " << cores_enviado<<": SPILL";
            }
        }
        if(i != 0){
            cout << endl;
        }
        cores_enviado --;
    }
    
    
    //Vertice *v = grafo->getVerticeFromGrafo(2);
    //cout << v->getGrau() << endl;
    return 0;
}
