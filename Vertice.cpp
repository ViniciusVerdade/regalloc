#include "Vertice.h"

Vertice::Vertice(int n_reg,int cor,bool eh_spill,int grau){
    this->n_reg = n_reg;
    this->cor = cor;
    this->eh_spill = eh_spill;
    this->grau = grau;
}

Vertice::~Vertice(){
//    cout << "Dando free no Vertice";
}

bool Vertice::reg_eh_spill(){
    return this->eh_spill;
}
void Vertice::addVerticeAdjacente(Vertice * vadd){

    vector<Vertice*>::iterator pos = this->verticesAdjacentes.begin();
    int size = this->verticesAdjacentes.size();
    int i;

    for (i = 0; i < size; i++){
        Vertice * v = this->verticesAdjacentes.at(i);
        if(vadd->getNreg() <= v->getNreg()){
            //v->setGrau(v->getGrau() + 1);
            this->grau++;
            this->verticesAdjacentes.insert(pos+i,vadd);
            return;
        }
    }
    this->grau++;
    this->verticesAdjacentes.push_back(vadd);
}


//GETTERS
int Vertice::getGrau(){
    return this->grau;
}

int Vertice::getNreg(){
    return this->n_reg;
}

int Vertice::getCor(){
    return this->cor;
}

vector<Vertice*> Vertice::getAdjacentes(){
    return this->verticesAdjacentes;
}
//SETTERS
void Vertice::setGrau(int grau){
    this->grau = grau;
}

void Vertice::setNreg(int n_reg){
    this->n_reg = n_reg;
}

void Vertice::setCor(int cor){
    this->cor = cor;
}

void Vertice::setEhSpill(bool s){
    this->eh_spill = s;
}
