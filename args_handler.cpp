#include "args_handler.h"

enum{
    NREG,
    INTERF
};

int numeroDeCores(string text){
    text = text.substr(2);
    int k = stoi(text);
    return k;
}

//Monta o grafo de analise dado o arquivo de entrada inserindo os vertices e criando as arestas de interferência
//TESTAR SE ESTÁ CORRETO
Grafo * montandoGrafoEntrada(vector<string> args,string nome_grafo, int k, int k_enviado){
    Grafo *grafo = new Grafo(nome_grafo);
    int i;
    int size = args.size();
    
    //loop criando os vertices de "k"
    for(i = 0; i < k_enviado; i++){
        Vertice *v = new Vertice(i,i,false,0);
        grafo->addVerticeInGrafo(v);
    }
    //loop criando com as interferencias
    for(i = 0; i < size; i++){
        vector<string> registers = formatandoEntrada(args.at(i));
        
        Vertice *v = grafo->temVerticeNReg(stoi(registers.at(NREG))); //se tiver retorna, se não tiver dá nulo

        if(!v){
            v = new Vertice(stoi(registers.at(NREG)),-1,false,0); 
            grafo->addVerticeInGrafo(v);
            
        }
        
        vector<string> interferencias = splitComDelimitador(registers.at(INTERF)," ");
        int j;
        int n_interferencias = interferencias.size();
        for(j = 0; j < n_interferencias; j++){
            int n_reg_interf = stoi(interferencias.at(j));
    
            Vertice *v_interf = grafo->temVerticeNReg(n_reg_interf); //se tiver retorna, se não tiver dá nulo

            if(!v_interf){
                v_interf = new Vertice(n_reg_interf,-1,false,0); 
                grafo->addVerticeInGrafo(v_interf);
                
            }
            

            v->addVerticeAdjacente(v_interf);
            //v_interf->addVerticeAdjacente(v);
        }
    }
    return grafo;
}

//envia string e uma string que deseja utilizar como separador
vector<string> splitComDelimitador(string s, string del){
    vector<string> splitted;

    size_t pos = 0;
    string token;
    //cout << s << endl;
    while ((pos = s.find(del)) != string::npos) {
        token = s.substr(0, pos);
        splitted.push_back(token);
        s.erase(0, pos + del.length());
    }
    
    splitted.push_back(s);
    return splitted;
}

vector<string> formatandoEntrada(string reg_line){
    vector<string> vect_regs;
    string delimiter = " --> ";
    size_t pos = 0;
    pos = reg_line.find(delimiter);
    
    vect_regs.push_back(reg_line.substr(0, pos));
    
    reg_line.erase(0, pos + delimiter.length());

    vect_regs.push_back(reg_line);
    return vect_regs;
}