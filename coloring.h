#ifndef COLORING_H
#define COLORING_H


#include <iostream>
#include <vector>
#include <string>
#include<fstream>
#include <algorithm>
#include "Grafo.h"
#include "Vertice.h"
using namespace std;

vector<Vertice *> simplify(Grafo* grafo, int k,int k_enviado);

bool select_assign(Grafo* grafo, vector<Vertice *> pilha_remocao, int k,int k_enviado);

#endif