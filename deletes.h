#ifndef DELETES_H
#define DELETES_H


#include <iostream>
#include <vector>
#include <string>
#include<fstream>
#include "Grafo.h"
#include "Vertice.h"
using namespace std;

void destroy_graph(Grafo* grafo);

#endif