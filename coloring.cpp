#include "coloring.h"

vector<Vertice *> simplify(Grafo* grafo,int k,int k_enviado){
    int maior_grau, menor_grau;
    int n_reg_maior, n_reg_menor;
    int last_pushed = -1;

    int i,j, size = grafo->getVertices().size();

    vector<Vertice *> pilha_de_remocao; 
    
    cout << "K = " << k << endl << endl;
    
    //*passando checando o menor grau
    //cout << "k_enviado: "<<k_enviado<<" ||k: "<<k<<endl;
    int fl_size = size - (k_enviado - k);
    for(j = k; j < fl_size; j++){

        maior_grau = -1;
        menor_grau = __INT_MAX__;
        n_reg_menor = __INT_MAX__;

        for(i = k; i < size; i++){
            Vertice * v = grafo->getVertices().at(i);
            
            int grau_v_atual = v->getGrau();
            int nreg_v_atual = v->getNreg();
            if(grau_v_atual > -1){//checa se o reg está no grafo
                if(nreg_v_atual >= k_enviado){
                        //?cout <<"nreg atual: " <<nreg_v_atual << endl;
                    if (nreg_v_atual != last_pushed){//checa se o nreg analisado é igual ao anterior
                        if(grau_v_atual < k){//se for menor que k, não precisa ver o maior grau, pois não terá chance de ser candida a spill
                            if(grau_v_atual < menor_grau){//checa se o grau do vertice atual é menor que o menor dentre os já analisados
                                menor_grau = grau_v_atual;
                                n_reg_menor = nreg_v_atual;
                                //cout << "grau < k : " << nreg_v_atual << endl;
                            }else{
                                //cout << "menor_grau : " << menor_grau << endl;
                            }
                        }else{//se for igual ou maior que k, tem chance de ser candidato a spill, então...
                            if(grau_v_atual > maior_grau){//checa se o grau deste vertice é maior q o maior grau
                                maior_grau = grau_v_atual;
                                n_reg_maior = nreg_v_atual;
                                //cout << "grau >= k : " << nreg_v_atual << endl;
                            }
                        }
                    }else{
                        break;
                    }
                }
            }
            //cout << endl;
        }

        if(menor_grau == __INT_MAX__){//se for o intmax quer dizer que nenhum grau de vertice do grafo é menor que k
            if (n_reg_maior != last_pushed){
                cout << "Push: " << n_reg_maior << " *" << endl;
                Vertice *v = grafo->temVerticeNReg(n_reg_maior);
                v->setGrau(-1);
                int n, size_adj = v->getAdjacentes().size();

                for(n = 0; n < size_adj; n++){//diminuindo grau dos adjacentes
                    Vertice *vAdj = v->getAdjacentes().at(n);
                    //if(vAdj->getNreg() >= k_enviado){//? SE NÃO FUNCIONAR, TIRAR E TESTAR O LOOP NO INICIO DO
                        vAdj->setGrau(vAdj->getGrau() - 1);
                    //}
                }
                last_pushed = v->getNreg();
                pilha_de_remocao.push_back(v);
                v->setEhSpill(true);
            }else{
                break;
            }

        }else{//se for diferente, quer dizer que em algum momento algum reg teve grau menor que k
            if (n_reg_menor != last_pushed){
                cout << "Push: " << n_reg_menor <<endl;
                Vertice *v = grafo->temVerticeNReg(n_reg_menor);
                v->setGrau(-1);
                int n, size_adj = v->getAdjacentes().size();

                for(n = 0; n < size_adj; n++){//diminuindo grau dos adjacentes
                    Vertice *vAdj = v->getAdjacentes().at(n);
                    vAdj->setGrau(vAdj->getGrau() - 1);
                }
                last_pushed = v->getNreg();
                pilha_de_remocao.push_back(v);
            }else{
                break;
            }
        }
    
    }
    return pilha_de_remocao;
}

bool select_assign(Grafo* grafo, vector<Vertice *> pilha_remocao, int k,int k_enviado){
    int i,size = pilha_remocao.size();
    int j, size_adj;

    for(i = 0; i < k; i++){ //setando os graus de cor pra "0"
        Vertice *v = grafo->getVertices().at(i);
        //if(k == 5){
        //    cout << "NREG: "<< v->getNreg() << " || GRAU: "<<v->getGrau()/*<<" || NREG_ADJ: "<<vadj->getNreg()<<" || GRAU_ADJ: "<<grauadj */<< endl;
        //}
        v->setGrau(0);
    }

    for(i = size-1; i >= 0; i--){//*PEGANDO O PRIMEIRO DA PILHA
        Vertice *v = pilha_remocao.at(i);
        v->setGrau(0);

        vector<int> cores_indisp;

        size_adj = v->getAdjacentes().size();

        for(j = 0; j < size_adj; j++){//*PEGANDO OS ADJACENTES PARA VER O GRAU E AS CORES
            Vertice *vadj = v->getAdjacentes().at(j);
            int grauadj = vadj->getGrau();
            if(k == 5){
                //cout << "NREG: "<< v->getNreg() << " || GRAU: "<<v->getGrau()<<" || NREG_ADJ: "<<vadj->getNreg()<<" || GRAU_ADJ: "<<grauadj <<"|| ADJ_COR: "<<vadj->getCor()<< endl;
            }
            if(grauadj >= 0){

                //if(vadj->getNreg() >= k){//*checa se é um registrador virtual
                vadj->setGrau(grauadj + 1);
                v->setGrau(v->getGrau() + 1);
                cores_indisp.push_back(vadj->getCor());
                //}

            }

        }
        if(v->reg_eh_spill()){//*se ele é candidato a spill
            if(v->getGrau() >= k) {//*deu spill
                bool not_spill = false;
                for(j = 0;j < k; j++){
                    if(k == 5){
                        //cout << j << endl;
                    }
                    vector<int>::iterator it = find(cores_indisp.begin(), cores_indisp.end(), j) ;
                    if ( !( it != cores_indisp.end() ) ){
                        v->setCor(j);
                        not_spill = true;
                        break;
                    }
                }
                if(not_spill){
                    cout << "Pop: "<<v->getNreg()<<" -> "<<j<<endl;
                }else{
                    cout << "Pop: "<<v->getNreg()<<" -> NO COLOR AVAILABLE"<<endl;
                    return not_spill;
                }
            }else{//* é candidado, mas não deu spill
                for(j = 0;j < k; j++){
                    if(k == 5){
                        //cout << j << endl;
                    }
                    vector<int>::iterator it = find(cores_indisp.begin(), cores_indisp.end(), j) ;
                    if ( !( it != cores_indisp.end() ) ){
                        v->setCor(j);
                        break;
                    }
                }
                cout << "Pop: "<<v->getNreg()<<" -> "<<j<<endl;
            }
        }else{//* caso não tenha dado spill
            for(j = 0;j < k; j++){
                if(k == 5){
                    //cout << j << endl;
                }
                vector<int>::iterator it = find(cores_indisp.begin(), cores_indisp.end(), j) ;
                if ( !( it != cores_indisp.end() ) ){
                    v->setCor(j);
                    break;
                }
            }
            cout << "Pop: "<<v->getNreg()<<" -> "<<j<<endl;
        }

    }
    return true;
} 