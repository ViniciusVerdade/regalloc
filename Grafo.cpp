#include "Grafo.h"
#include "Vertice.h"

Grafo::Grafo(string nome){
    this->qtd_vertices = 0;
    this->nome = nome;
    //cout << "Grafo de nome:" << this->nome;
}

Grafo::~Grafo(){
    //cout << "Dando free no Grafo...\n";
}

vector<Vertice*> Grafo::getVertices(){
    return this->vertices;
}

string Grafo::getNome(){
    return this->nome;
}

int Grafo::getQuantidadeVertices(){
    return this->qtd_vertices;
}

Vertice* Grafo::getVerticeFromGrafo(int n_reg){
    int i = 0;
    //cout<< "peguei o v!"<< endl;
    int size = this->vertices.size();
    for (i = 0; i < size; i++){
        
        Vertice *v = this->vertices.at(i);
        if(v->getNreg() == n_reg){
            break;
        }
    }
    return this->vertices.at(i);
}

void Grafo::addVerticeInGrafo(Vertice *vadd){

    vector<Vertice*>::iterator pos = this->vertices.begin();
    
    //cout << pos << ": esta e a posicao" << endl;
    int size = this->vertices.size();
    int i;

    for (i = 0; i < size; i++){

        Vertice* v = this->vertices.at(i);

        if(vadd->getNreg() <= v->getNreg()){
            //if(pos){
                this->vertices.insert(pos+i,vadd);
                return;
            //}else{
            //    cout << "pos é nulo" << endl;
            //}
        }

    }
    this->vertices.push_back(vadd);
    this->qtd_vertices++;
}

Vertice * Grafo::temVerticeNReg(int n_reg){
    
    int size = this->vertices.size();
    int i;

    for (i = 0; i < size; i++){
        Vertice* v = this->vertices.at(i);

        if(n_reg == v->getNreg() ){
            return v;
        }
    }
    return nullptr;
}

void Grafo::printElementosGrafo(){
    int size = this->vertices.size();
    int i;
    cout << "Grafo: " << this->nome << endl;
    cout << "Este grafo possui: " << this->qtd_vertices << " vertices;";
    for (i = 0; i < size; i++){
        Vertice* v = this->vertices.at(i);
        cout << "Informações do vertice: " << v->getNreg() << endl;
        cout << "adjacentes:" ;
        int j,size_adj = v->getAdjacentes().size();
        cout << "( tamanho: "<< size_adj <<" )";
        for( j = 0; j < size_adj; j++){
            Vertice * v2 = v->getAdjacentes().at(j);
            cout << v2->getNreg() <<" ";
        }
        cout << endl << "grau: " << v->getGrau() << endl;
        cout << endl;
    }
}