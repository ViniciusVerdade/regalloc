#ifndef VERTICE_H
#define VERTICE_H

#include <iostream>
#include <vector>
using namespace std;

class Vertice
{
    public:
        Vertice(int n_reg,int cor,bool eh_spill,int grau);
        ~Vertice();
        int getGrau();
        int getNreg();
        int getCor();
        vector<Vertice *> getAdjacentes();
        void setGrau(int grau);
        void setNreg(int n_reg);
        void setCor(int cor);
        void setEhSpill(bool s);
        void addVerticeAdjacente(Vertice * v);
        bool reg_eh_spill();

    private:
        int n_reg;
        int cor;
        bool eh_spill;
        int grau;
        vector<Vertice*> verticesAdjacentes;
        
};
#endif