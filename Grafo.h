#ifndef GRAFO_H
#define GRAFO_H

#include <iostream>
#include "Vertice.h"
#include <vector>
using namespace std;

class Grafo
{
    public:
        Grafo(string nome);
        ~Grafo();
        vector<Vertice *> getVertices();
        string getNome();
        int getQuantidadeVertices();
        Vertice* getVerticeFromGrafo(int n_reg);
        void addVerticeInGrafo(Vertice *v);
        Vertice* temVerticeNReg(int n_reg);
        void printElementosGrafo();

    private:
        int qtd_vertices;
        string nome;
        vector<Vertice*> vertices;
        
};
#endif