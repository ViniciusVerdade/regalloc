#ifndef ARGS_HANDLER_H
#define ARGS_HANDLER_H


#include <iostream>
#include <vector>
#include <string>
#include<fstream>
#include "Grafo.h"
#include "Vertice.h"
using namespace std;

int numeroDeCores(string text);
Grafo * montandoGrafoEntrada(vector<string> args,string nome_grafo, int k,int k_enviado);
vector<string> formatandoEntrada(string reg_line);
vector<string> splitComDelimitador(string s, string del);

#endif